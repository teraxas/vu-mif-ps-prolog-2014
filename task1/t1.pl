%1111025	2	3	26	28

%Užduoties formulavimas. Duom. bazėje saugomi duomenys apie asmenis
%ir jų giminystės ryšius faktais:
%          asmuo(Vardas, Lytis, Amžius, Pomėgis);
%          mama (Mama,  Vaikas).
%          pora (Vyras, Žmona);

%Apibrėžti šiuos predikatus:
%dukra(X, Y) - "Įprastas apibrėžimas";
%broliai(X, Y);
%bendraaziai(X, Y);
%vps(X) : "X - vyras pačiame žydėjime(iš tam tikro amžiaus intervalo)";

asmuo(ona, m, 70, bulves).
asmuo(zigmas, v, 75, kortos).
asmuo(jonas, v, 21, robotai).
asmuo(petras, v, 20, masinos).
asmuo(simas, v, 25, f-1).
asmuo(robertas, v, 45, muzika).
asmuo(lina, m, 45, filmai).
asmuo(justina, m, 24, zaidimai).
pora(lina, robertas).
pora(ona, zigmas).
pora(justina, simas).
mama(ona, robertas).
mama(lina, justina).
mama(lina, petras).
mama(lina, jonas).

dukra(Mother, Child):-
	mama(Mother, Child),
	asmuo(Child, m, _, _).

broliai(Brolis1, Brolis2):-
	yraVyras(Brolis1),
	yraVyras(Brolis2),
	Brolis1 \= Brolis2,
	tosPaciosMamosVaikai(Brolis1, Brolis2).

bendraamziai(Asmuo1, Asmuo2):-
	asmuo(Asmuo1, _, Amzius, _),
	asmuo(Asmuo2, _, Amzius, _).

vps(Asmuo):-
	asmuo(Asmuo, v, Amzius, _),
	between(19, 30, Amzius).

vedusi(Moteris):-
	asmuo(Moteris, m, _, _),
	pora(Moteris, _).

tosPaciosMamosVaikai(Asmuo1, Asmuo2):-
	mama(Mother, Asmuo1),
	mama(Mother, Asmuo2).

yraVyras(Asmuo):-
	asmuo(Asmuo, v, _, _).
