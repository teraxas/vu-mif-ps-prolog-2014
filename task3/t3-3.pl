/*
3.11. skirtumas (S1,S2 ,R):
Sąrašas R susideda iš duotojo sąrašo S1 elementų,
kurie nepriklauso kitam duotąjam sąrašui S2.
	Pvz. goal: skirtumas ([a,b,c,d], [d,e], R).
	R = [a,b,c].
*/

member(X, [A | L]) :-
    X = A ; 
    member(X, L).

difference([A | L1], L2, Diff) :-
	member(A, L2);
	member(A, Diff),
	(
		L1 = [];
		difference(L1, L2, Diff)
	).