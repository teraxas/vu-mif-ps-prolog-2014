/*
2.2. maxlyg(S,M):
Skaičius M - didziausias lyginis skaičių sąrašo S elementas.
	Pvz. goal: maxlyg([4,10,-2,-1,23],M).
	M =10.

*/

maxlyg([X | L], M):-
	0 is X mod 2, % fail if not even
  	( 
  		maxlyg(L, T), 
  		T > X, 
  		!, 
  		M = T ; M = X
  	).
	
maxlyg([_|L], M) :-
  	maxlyg(L, M).