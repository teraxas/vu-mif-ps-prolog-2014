/*
4.8. posarasis(X,Y):
Sąrašas Y susideda iš tam tikro skaičiaus X elementų išdėstytų ta pačia
tvarka, kaip ir sąraše Y (kitaip tariant, iš sąrašo X išmetus tam tikrus
elementus, galime gauti sąrašą Y)
	Pvz. goal:posarasis ([1,8,3,5],[1,3]).
	true.
*/

posarasis([A | L1], [B | L2]):-
	(
		A = B,
		(
			L2 = [];
			(
				not(L1 = []),
				posarasis(L1, L2)
			)
		)
	);
	(
		not(L1 = []),
		posarasis(L1, [B | L2])
	).