/*
1.4. progr(S):
Duoto sveikųjų skaičių sąrašo S elementai sudaro aritmetinę progresiją.
	Pvz. goal: progr([1,4,7,10]).
	true.
*/

skirtumas(A, B, Diff):-
	Diff is B - A.

progresija([A, B | L]):-
	skirtumas(A, B, Diff),
	progresijaInner(L, B, Diff).

progresijaInner([A | L], Prev, Diff):-
	skirtumas(Prev, A, Diff),
	(
		L = [];
		progresijaInner(L, A, Diff)
	).