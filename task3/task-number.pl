
var(UNR,X,Y,Z,W):-
	  X is UNR mod  7  + 1,
	  Y is UNR mod  8  + 1,
	  Z is UNR mod 11  + 1,
	  W is UNR mod  9  + 1.

/*
	?- var(0241, X, Y, Z, W).
	X = 4,
	Y = 2,
	Z = 11,
	W = 8.


	Variantai:
		1.4, 2.2, 3.11, 4.8

1.4. progr(S):
Duoto sveikųjų skaičių sąrašo S elementai sudaro aritmetinę progresiją.
	Pvz. goal: progr([1,4,7,10]).
	true.

2.2. maxlyg(S,M):
Skaičius M - didziausias lyginis skaičių sąrašo S elementas.
	Pvz. goal: maxlyg([4,10,-2,-1,23],M).
	M =10.

3.11. skirtumas (S1,S2 ,R):
Sąrašas R susideda iš duotojo sąrašo S1 elementų,
kurie nepriklauso kitam duotąjam sąrašui S2.
	Pvz. goal: skirtumas ([a,b,c,d], [d,e], R).
	R = [a,b,c].

4.8. posarasis(X,Y):
Sąrašas Y susideda iš tam tikro skaičiaus X elementų išdėstytų ta pačia
tvarka, kaip ir sąraše Y (kitaip tariant, iš sąrašo X išmetus tam tikrus
elementus, galime gauti sąrašą Y)
	Pvz. goal:posarasis ([1,8,3,5],[1,3]).
	true.

*/