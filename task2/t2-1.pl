% 2. Duoti faktai pavidalu
%                   studentas(Vardas, Kursas),
%                   yraVyresnis(Studento_vardas1, Studento_vardas2).
% Santykis "Būti vyresniu" - tranzityvus. (Todėl į faktų "yraVyresnis" bazę
% neįtraukiami tie faktai kurie sektų iš tranzityvumo saryšio).
% 
% Apibrėžti predikatus:
%        c)"A vyresnis negu to paties kurso studentas B";

studentas(jonas, 4).
studentas(petras, 3).
studentas(sigitas, 3).
studentas(zigmas, 3).
studentas(arunas, 2).
studentas(simas, 1).

yraVyresnis(jonas, petras).
yraVyresnis(petras, sigitas).
yraVyresnis(sigitas, zigmas).
yraVyresnis(zigmas, arunas).
yraVyresnis(arunas, simas).

yraVyresnis1(x, y):-
	yraVyresnis(x, Z),
	yraVyresnis(y, Z).

yraVyresnisUzKursioka(stud1, stud2):-
	studentas(stud1, Kursas),
	studentas(stud2, Kursas),
	yraVyresnis(stud1, stud2).