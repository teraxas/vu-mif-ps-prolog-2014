/*
	Karolis Jocevičius PS1

	Duotas miestus jungiančių kelių tinklas. Keliai vienakrypčiai,
	NESUDARANTYS ciklų. Žinomi atstumai tarp miestų. Tai išreiškiama faktais
	    kelias (Miestas1, Miestas2, Atstumas).
	Apibrėžti predikatą "galima pervažiuoti  iš miesto X i miestą Y

	b) nuvažiavus lygiai L kilometrų;
*/

kelias(vilnius, kaunas, 100).
kelias(kaunas, vilnius, 100).
kelias(kaunas, klaipeda, 200).
kelias(klaipeda, kaunas, 200).
kelias(vilnius, siauliai, 250).
kelias(siauliai, vilnius, 250).
kelias(kaunas, siauliai, 100).
kelias(siauliai, kaunas, 100).

/*
	vilnius --100-- kaunas --200-- klaipeda
	      \         /
	      250     100
	        \     /
	        siauliai 
*/

/*
galimaPervaziuoti(M1, M2):-
	kelias(M1, X, _),
	kelias(X, M2, _).
*/

galimaPervaziuotiPerAtstuma(M1, M2, ReqDistance, CurDistance):-
	M1 \= M2,
	kelias(M1, X, Distance),
	IncDistance is CurDistance + Distance,
	(
		(
			IncDistance < ReqDistance,
			galimaPervaziuotiPerAtstuma(X, M2, ReqDistance, IncDistance)
		);
		(	
			IncDistance = ReqDistance,
			X = M2
		)
		
	).

galimaPervaziuotiPerAtstumaLimited(M1, M2, ReqDistance):-
	(M1 = M2, ReqDistance = 0);
	kelias(M1, M2, ReqDistance);
	galimaPervaziuotiPerAtstuma(M1, M2, ReqDistance, 0).