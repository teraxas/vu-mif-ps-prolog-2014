/*
	Karolis Jocevičius PS1

	Faktais užrašyta logikos algebros konjunkcijos bei neiginio reikšmių
	lentelė. Apibrėžti predikatus
	b) "disjunkcija"
*/

neiginys('true', 'false').
neiginys('false', 'true').

konjunkcija('true', 'true', 'true').
konjunkcija('true', 'false', 'false').
konjunkcija('false', 'true', 'false').
konjunkcija('false', 'false', 'false').

/*
	 logine daugyba - konjunkcija - and
	 logine sudetis - disjunkcija - or
		
		a and b
		true true -> true
		false true -> false
		true false -> false
		false false -> false
		
		a or b
		true true -> true
		false true -> true
		true false -> true
		false false -> false
		
		ne(ne(a) ir ne(b)) -> logine sudetis per neigima ir konjunkcija
		true true -> true
		true false -> true
		false true -> true
		false false -> false
*/

disjunkcija(A, B, Rez):-
	neiginys(A, NeigA),
	neiginys(B, NeigB),
	konjunkcija(NeigA, NeigB, KonRez),
	neiginys(KonRez, Rez). 